## [Download Log v1.0](https://bitbucket.org/jeffbohmann/log/downloads/log-1.0.jar) - A Logger wrapper for slf4j (slf4j-api-1.7.x). 

### Benefits/Restrictions
1. No need to specify the class for the Logger.
1. Log instances are validated to ensure they are initialized statically.
1. Four logging levels: DEBUG, INFO, WARN, and ERROR.
    * No debate over DEBUG vs TRACE
    * Why FATAL, when you'll be crashing anyway? Just use ERROR.
1. No Throwable signature for DEBUG and INFO.
    * Enforces usage of WARN and ERROR only for exceptions.

### Examples
```
//static initialization is required
//initializing(owning) class is automatically passed to the underlying Logger
static final Log log = Log.get(); 
```
```
log.info("Server startup completed in {}{}.", serverInitTime, timeUnit); 
```
```
...
catch(FileReadException e) {
    if(retryCount <= RETRY_LIMIT) {
        log.warn("Error reading from deployment directory, retrying...", e);
    }
    else {
        log.error("Failed to read from deployment directory.", e);
        throw new DeploymentDirectoryUnreadable(e);
    }
}
```